# Fedora Ansible Test Image

Fedora Docker containers for Ansible playbook and role testing.

## How to Use

  1. [Install Docker](https://docs.docker.com/engine/installation/).
  2. Pull this image from Ninux Gitlab Docker Registry:
  
  ```bash
    # Fedora 27
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-fedora:27
    
    # Fedora 28
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-fedora:28
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-fedora:latest
  ```
   
  3. Run a container from the image:
    
  ```bash
    # Fedora 27
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-fedora:27 /usr/lib/systemd/systemd
    
    # Fedora 28
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-fedora:28 /usr/lib/systemd/systemd
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-fedora:latest /usr/lib/systemd/systemd
  ```

  4. Use Ansible inside the container:
    
  ```bash
    docker exec --tty [container_id] env TERM=xterm ansible --version
    docker exec --tty [container_id] env TERM=xterm ansible-playbook /path/to/ansible/playbook.yml --syntax-check
  ```
    